module.exports = function(grunt) {
  
  const sass = require('node-sass');

  require('load-grunt-tasks')(grunt, 
    {scope: 'devDependencies'});

  grunt.initConfig({
    sass: {
      options: {
          sourceMap: true,
          implementation: sass,
          style: "compressed"
      },
      dist: {
        files: {
          "src/style.min.css": "src/scss/index.scss"
        }
      }
    },
    watch: {
      style : {
        files : ['src/scss/*.scss','src/*/*/*.scss','src/*/*/*/*.scss'],
        tasks : ['sass']
      }
    }
  });
  grunt.registerTask('default', [
    'watch:style'
  ]);
};