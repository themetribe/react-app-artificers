import React, { Component } from 'react';
import Avatar from '../../components/avatar/avatar';
import Burger from '../../components/burger/burger';

export default class splashScreen extends Component {
  render() {
    return (
      <div id="splashScreen" className="start">
        <Burger />
      	<Avatar />
      	<h1>
      		<small>The</small>Artificer
      		<sub>mobile * web * designs</sub>
      	</h1>
      </div>
    );
  }
}