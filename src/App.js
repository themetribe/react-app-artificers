import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./style.min.css";

import splashScreen from "./screens/SplashScreen/splashScreen"

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={splashScreen} />
      </Router>
    );
  }
}

export default App;
