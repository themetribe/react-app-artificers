import React, { Component } from 'react';

export default class burger extends Component {
	state = {
		menuStatus : null // show/null
	}
	render() {
		const {menuStatus} = this.state;
		return (
			<div>
				<span id="menu-bg" className={this.state.menuStatus}></span>
				<div id="burger">
					<input type="checkbox" onClick={() => this.toggleMenuStatus} />
					<span></span>
				    <span></span>
				    <span></span>
				</div>
		  	</div>
		);
	}

	toggleMenuStatus() {
		const {menuStatus} = this.state;
		this.setState({menuStatus : menuStatus === null ? 'show' : null });
	}
}